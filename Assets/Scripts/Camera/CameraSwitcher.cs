﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraSwitcher : MonoBehaviour
{
    public bool aiming = false;
    private bool aimed = false;
    public GameObject ThirdPersonCamera;
    public GameObject AimingCameara;

    private CinemachineFreeLook vCam;


    // Start is called before the first frame update
    void Start()
    {
        vCam = ThirdPersonCamera.GetComponent<CinemachineFreeLook>();
    }

    // Update is called once per frame
    void Update()
    {
        GetInput();
        SwitchCams();

    }

    private void SwitchCams()
    {
        if (aiming)
        {
            if (!aimed)
            {
                float camDir = vCam.m_XAxis.Value;

                transform.rotation = Quaternion.Euler(0, camDir, 0);

                aimed = true;
            }


            ThirdPersonCamera.SetActive(false);
            AimingCameara.SetActive(true);
        }
        else
        {
            ThirdPersonCamera.SetActive(true);
            AimingCameara.SetActive(false);
            aimed = false;

        }
    }


    private void GetInput()
    {
        // aiming
        if (Input.GetKey(KeyCode.Mouse1)) 
        {
            aiming = true;
        }
        else
        {
            aiming = false;
        }
        // fire
        if(Input.GetKeyDown(KeyCode.Mouse0))
        {

        }
    }
}
