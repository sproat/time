﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimCameraController : MonoBehaviour
{
    public float rotationPower = 3f;
    public GameObject followTransform;
    public GameObject player;
    Quaternion nextRotation;

    Vector2 rotation = Vector2.zero;
    void Start ()
    {
        rotation.y = followTransform.transform.eulerAngles.y;
    }


    // Update is called once per frame
    void Update()
    {

        rotation.y += Input.GetAxis("Mouse X") * rotationPower;
        rotation.x += -Input.GetAxis("Mouse Y") * rotationPower;
        rotation.x = Mathf.Clamp(rotation.x, -60, 60);
        followTransform.transform.localRotation = Quaternion.Euler(rotation.x, 0, 0);
        player.transform.eulerAngles = new Vector2(0, rotation.y);

    }
}
