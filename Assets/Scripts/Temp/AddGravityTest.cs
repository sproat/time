﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddGravityTest : MonoBehaviour
{
    [SerializeField] GameObject modify;


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Rigidbody gravity = modify.GetComponent<Rigidbody>();
            gravity.useGravity = true;

            Destroy(gameObject);
        }
    }

}
