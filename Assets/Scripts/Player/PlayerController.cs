﻿using Chronos;
using System;
using System.Collections;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    //Ground
    [Header("Ground")]
    public float groundSpeed = 3.44f;
    public float runSpeedMod = 1.5f;
    public float crouchingModifer = 0.5f;
    public float grAccel = 200f;

    //Air
    [Header("Air")]
    public float airSpeed = 3f;
    public float airAccel = 0.3f;
    public float airTargetSpeed = 15f;

    //Wall
    [Header("Wall")]
    public float wallAccel = 5f;
    public float wallStickiness = 10f;
    public float wallStickDistance = 0.5f;
    public float wallRunTilt = 15f;
    public float wallFloorBarrier = 40f;
    public float wallBanTime = 0.3f;
    public float wallrunTime = 4f;
    public float wallRunJumpDampeningModifer = 0.6f;
    public float wallRunVertDamp = 0.6f;
    public float wallRunMinSpeed = 15f;
    public float wallRunUpForce = 1000f;
    public float wallRunDownForce = 1000f;

    //Jump
    [Header("Jump")]
    public float jumpForce = 6f;
    public float dashForce = 4f;
    public float bhopLeniency = 0.1f;
    public float dJumpBaseSpd = 8;

    //Crouch & Slide
    [Header("Crouch & Slide")]
    public float slideForce = 400;
    public float slideCounterMovement = 0.2f;
    public float slideFovIncrease = 10;
    public float camLerpSpeed = 10f;
    private float oldFovValue = 90;
    private float slideFovValue = 10f;

    private Vector3 crouchScale = new Vector3(1, 0.5f, 1);
    private Vector3 playerScale;

    // Private || Not Shown in inspector
    bool jump;
    [HideInInspector] public bool canDJump = false;

    bool playerInput = true;
    bool running = false;
    bool grounded = false;
    bool sliding = false;
    bool crouchedInAir = false;
    bool graceTimerStarted = false;
    bool inAir = false;
    bool appliedJump = false;
    bool appliedDoubleJump = false;
    float angleToWall;
    Vector3 moveDir;

    [HideInInspector] public bool crouched = false;
    [HideInInspector] public bool crouching = false;

    Vector3 groundNormal;
    Vector3 bannedGroundNormal;

    Vector3 dir;
    Vector3 spid;
    Vector3 lastPosition = Vector3.zero;
    Vector3 rayDir;
    Vector3 climbTargetPos;

    float currSpeed = 0;
    float vertAxisInput = 0;
    float horizAxisInput = 0;
    float lerpT = 0;
    float dynamFricOld;
    float turnSmoothVelocityWalk;
    float turnSmoothVelocityAir;

    float targetAngle;
    float lookAngle;
    private enum State
    {
        Walking,
        Flying,
        Wallruning,
        Climbing
    }
    private State state;

    //Timers
    private Timer bhopLenTimer;
    private Timer wallrunTimer;
    private Timer wallDetachTimer;
    private Timer wallBanTimer;
    private Timer jumpTimer;
    private Timer jumpGraceTimer;

    //GameObjects/References
    private Timeline time;
    private CapsuleCollider pm;
    private PlayerCameraController camCon;
    private Timeline timeline;
    private Animator animate;
    private Rigidbody climbRef;
    private CameraSwitcher camSwitch;

    [Header("Public References")]
    public Collider wallRunCollider;
    public Transform cam;


    void Awake()
    {
        //Timers
        bhopLenTimer = gameObject.AddComponent<Timer>();
        bhopLenTimer.SetTime(bhopLeniency);

        wallBanTimer = gameObject.AddComponent<Timer>();
        wallBanTimer.SetTime(wallBanTime);

        wallDetachTimer = gameObject.AddComponent<Timer>();
        wallDetachTimer.SetTime(0.2f);

        wallrunTimer = gameObject.AddComponent<Timer>();
        wallrunTimer.SetTime(wallrunTime);

        jumpTimer = gameObject.AddComponent<Timer>();
        jumpTimer.SetTime(0.05f);

        jumpGraceTimer = gameObject.AddComponent<Timer>();
        jumpGraceTimer.SetTime(0.4f);


        //GameObjects
        //camCon = cam.GetComponent<PlayerCameraController>();
        time = GetComponent<Timeline>();
        pm = GetComponent<CapsuleCollider>();
        //camCon.SetTilt(0);
        timeline = GetComponent<Timeline>();
        animate = GetComponentInChildren<Animator>();
        climbRef = GetComponent<Rigidbody>();
        camSwitch = GetComponent<CameraSwitcher>();

        //Setting up beggining state
        EnterFlying();
        canDJump = true;
        pm.material.dynamicFriction = 0;
        pm.material.frictionCombine = PhysicMaterialCombine.Minimum;
        playerScale = transform.localScale;

        //oldFovValue = cam.fieldOfView;
       // slideFovValue += oldFovValue;
        dynamFricOld = pm.material.dynamicFriction;


        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;


    }

    void Update()
    {
        if (time.timeScale > 0)
        {
            dir = Direction();
            CrouchBoostJump();
            CheckGrounded();
            SetAnimation();



            Debug.DrawRay(new Vector3(transform.position.x, transform.position.y + pm.height / 2.5f, transform.position.z), rayDir * 0.75f, Color.red);
            Debug.DrawRay(new Vector3(transform.position.x, transform.position.y - 0.25f, transform.position.z), rayDir * 0.75f, Color.red);
            Debug.DrawRay(new Vector3(transform.position.x, transform.position.y - pm.height / 2.5f, transform.position.z), rayDir * 0.75f, Color.red);

        }
    }

    private void CheckGrounded()
    {
        if (state != State.Wallruning)
        {
            Vector3 position = transform.position;
            Debug.DrawRay(position, Vector3.down * 1.25f, Color.red);
            RaycastHit hit;
            if (Physics.Raycast(position, Vector3.down, out hit, wallStickDistance))
            {
                grounded = true;
                groundNormal = hit.normal;
                EnterWalking();
            }
            else
            {
                grounded = false;
                EnterFlying();
            }
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawSphere(transform.position + transform.forward * 0.7f + transform.up * 1.25f, 0.5f); 
    }


    private void FixedUpdate()
    {

        if (time.timeScale > 0)
        {
            if (wallDetachTimer.Done() && !wallBanTimer.Done())
            {
                bannedGroundNormal = groundNormal;
            }
            else
            {
                bannedGroundNormal = Vector3.zero;
            }
            switch (state)
            {
                case State.Wallruning:
                    Wallrun(dir, groundSpeed, grAccel);
                    break;
                case State.Walking:
                    //camCon.SetTilt(0);
                    Walk(dir, groundSpeed, grAccel);
                    break;
                case State.Flying:
                    //camCon.SetTilt(0);
                    AirMove(dir, airSpeed, airAccel);
                    break;
                case State.Climbing:
                    Climb();
                    break;
            }
            jump = false;

            if (sliding)
            {
                //add counterforce
                time.rigidbody.AddForce(timeline.deltaTime * -time.rigidbody.velocity.normalized * slideCounterMovement);
            }
            else
            {
                pm.material.dynamicFriction = dynamFricOld;
                pm.material.frictionCombine = PhysicMaterialCombine.Average;
            }
            currSpeed = CurrentPlayerSpeed();
        }


    }

    void OnCollisionStay(Collision collision)
    {
        if (collision.contactCount > 0)
        {
            if (grounded == false)
            {
                foreach (ContactPoint contact in collision.contacts)
                {
                    if (contact.thisCollider == wallRunCollider && state != State.Walking && state != State.Climbing)
                    {
                        float angle = Vector3.Angle(contact.normal, Vector3.up);
                        if (angle > wallFloorBarrier && angle < 120f)
                        {
                            grounded = true;
                            groundNormal = contact.normal;
                            EnterWallrun();
                            return;
                        }
                    }
                }
            }
        }
    }

    void OnCollisionExit(Collision collision)
    {
        if (collision.contactCount == 0)
        {
            EnterFlying();
        }
    }

    private Vector3 Direction()
    {
        if (playerInput)
        {

            // prevent issues that arise when player uses horizontal innput while wall running
            if (state == State.Wallruning)
            {
                horizAxisInput = 0;
                vertAxisInput = Mathf.Clamp(Input.GetAxisRaw("Vertical"), 0, 1);
            }
            else
            {
                horizAxisInput = Input.GetAxisRaw("Horizontal");
                vertAxisInput = Input.GetAxisRaw("Vertical");
            }

            Vector3 direction = new Vector3(horizAxisInput, 0f, vertAxisInput);

            if (!camSwitch.aiming) // if we are aiming, make movement independent of camera direction
            {
                float angleSave = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + cam.eulerAngles.y;
                lookAngle = angleSave;
                if (direction.normalized.magnitude >= 0.1f)
                {
                    targetAngle = angleSave;
                    direction = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;
                }

                return direction;
            }
            else
            {
                return time.rigidbody.component.transform.TransformDirection(direction);
            }
        }
        else
        {
            return Vector3.zero;
        }
    }

    private void SetAnimation()
    {
        float moveV = vertAxisInput;
        float moveH = horizAxisInput;


        if (state == State.Wallruning)
        {

            animate.SetFloat("wallAngle", angleToWall);
            animate.SetBool("wallrunning", true);

        }
        else if (state == State.Climbing)
        {
            animate.SetBool("climb", true);
        }
        else
        {
            animate.SetBool("climb", false);
            animate.SetBool("wallrunning", false);
            // if player is sliding
            if (sliding)
            {
                animate.SetBool("sliding", true);
            }
            else if (!sliding)
            {
                animate.SetBool("sliding", false);

                // if player is crouching
                if (crouching)
                {
                    animate.SetBool("crouching", true);
                }
                else if (!crouching)
                {
                    animate.SetBool("crouching", false);
                }
            }


            // if player is not crouching/sliding and is running
            if (!crouching && !sliding && running)
            {
                if (moveV > 0) moveV = 2;
                else
                {
                    moveV = -2;
                }
            }
        }
       

        if (appliedJump)
        {
            animate.SetFloat("jumps", 1f);
            appliedJump = false;
        }

        if (appliedDoubleJump) //this means we used double jump
        {
            animate.SetFloat("jumps", 2f);
            animate.SetTrigger("doubleJumpUsed");
            appliedDoubleJump = false;
        }

        if (!grounded)
        {
            inAir = true;
            animate.SetBool("grounded", false);
        }
        else
        {
            animate.SetBool("grounded", true);

        }

        // check for landing on ground separtly from states since this can occur at various times
        if (grounded)
        {
            //this checks if we are grounded but the lended check knows we were also just in the air
            inAir = false;
            animate.SetFloat("jumps", 0);
        }

        animate.SetFloat("vertical_f", moveV, 1f, Time.deltaTime * 10f);
        animate.SetFloat("horizontal_f", moveH, 1f, Time.deltaTime * 10f);
        animate.SetFloat("playerVelocity", time.rigidbody.velocity.magnitude);


    }

    #region EnterState
    private void EnterWalking()
    {
        if (state != State.Walking)
        {
            // start slide if crouching in-air when you hit the ground
            if (crouching && time.rigidbody.velocity.magnitude > 0.5f)
            {
                Slide();
            }

            //SoundManagerScript.PlaySound("land");
            bhopLenTimer.StartTimer();
            state = State.Walking;
            playerInput = true;
        }
    }

    private void EnterFlying( bool wishFly = false)
    {
        grounded = false;
        if(state == State.Wallruning && VectorToWall().magnitude < wallStickDistance && !wishFly)
        {
            return;
        }
        else if (state != State.Flying)
        {
            wallBanTimer.StartTimer();
            canDJump = true;
            state = State.Flying;
            playerInput = true;
        }
    }

    private void EnterWallrun()
    {
        if (state != State.Wallruning && VectorToGround().magnitude > 0.2f && CanRunOnThisWall(bannedGroundNormal) && wallDetachTimer.Done())
        {
            wallrunTimer.StartTimer();
            canDJump = true;
            state = State.Wallruning;

            // remove y-axis velocity when grabbing on a wall
            Vector3 tmpVelocity = time.rigidbody.velocity;
            tmpVelocity.y = 0f;
            time.rigidbody.velocity = tmpVelocity;

        }
    }

    private void EnterClimbing()
    {
        if (state != State.Climbing)
        {
            //climbTargetPos = new Vector3(transform.position.x, transform.position.y + 1.5f, transform.position.z + 1.25f);
            climbTargetPos = transform.position + transform.forward * 0.7f + transform.up * 1.25f;
            state = State.Climbing;
        }
    }

    #endregion



    #region Movement

    private void CrouchBoostJump()
    {
        if (grounded && graceTimerStarted)
        {
            // reset grace timer when we hit the ground
            graceTimerStarted = false;
        }

        // Jumping
        if (Input.GetKeyDown(KeyCode.Space))
        {
            jump = true;
            if (grounded)
            {
                graceTimerStarted = true;
                jumpGraceTimer.done = true;
            }

        }
        else if (!grounded && !graceTimerStarted)
        {
            // here we want to add a "grace timer" to allow a jump after the player walks off a ledge without using their double jump
            jumpGraceTimer.StartTimer();
            graceTimerStarted = true;
        }

        // Sprinting (toggle)
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            if (running) running = false;
            else
            {
                running = true;
            }
            
        }

        //Crouching
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            //this looking confusing but the code needs to know when the button is pressed AND if it's being held down
            // bc of in-air crouching and sliding

            crouched = true;
        }
        if (Input.GetKeyUp(KeyCode.LeftControl))
        {
            StopCrouch();
            crouching = false;
        }
        // this is to check for sliding when you hit the ground since it only checks once when you crouch (oops..)
        if (crouchedInAir && crouching && !sliding && time.rigidbody.velocity.magnitude > groundSpeed)
        {
            Slide();
            crouchedInAir = false;
        }
    }

    private void Wallrun(Vector3 wishDir, float maxSpeed, float Acceleration)
    {
        if (LedgeClimb())
        {
            EnterClimbing();
        }
        if (jump)
        {
            Vector3 direction = new Vector3(groundNormal.x, 1, groundNormal.z);

            // wallRunJumpDampeningModifer slows down a jump from a wall, was too fast
            time.rigidbody.AddForce(direction * jumpForce * wallRunJumpDampeningModifer, ForceMode.Impulse);

            EnterFlying(true);
        }
        else if (wallrunTimer.Done())
        {
            //time.rigidbody.AddForce(groundNormal * 3f, ForceMode.Impulse);

            // Applies force to eject player off of wall in opposite direction to wall
            // This is difference to default approach which pushes player down

            time.rigidbody.AddForce(-VectorToWall() * (Mathf.Clamp(currSpeed * 6, 1, 3) * 20), ForceMode.Impulse);
            EnterFlying(true);
        }
        else
        {
            //Variables
            Vector3 distance = VectorToWall();

            // workaround to fix issue where looking up/down would effect wallrunning direction 
            wishDir = new Vector3(wishDir.x, 0, wishDir.z);

            wishDir = wishDir.normalized;
            wishDir = RotateToPlane(wishDir, -distance.normalized);
            wishDir *= wallAccel;


            Vector3 antiGravityForce = -Physics.gravity;
            Vector3 verticalDamping;

            //Calculating forces
            if (Mathf.Sign(time.rigidbody.velocity.y) != Mathf.Sign(wishDir.y) && time.rigidbody.velocity.y < 0)
            {
                verticalDamping = new Vector3(0, -time.rigidbody.velocity.y * 0.1f, 0);
                time.rigidbody.AddForce(verticalDamping * wallRunVertDamp, ForceMode.VelocityChange);
            }
            else
            {
                verticalDamping = new Vector3(0, -time.rigidbody.velocity.y * 0.5f, 0);
                time.rigidbody.AddForce(verticalDamping * wallRunVertDamp, ForceMode.Acceleration);

            }

            float timeLeft = wallrunTimer.CurrentTime();
            //provide upwards boost for first 0.4 secs if player is trying to move forwards
            if (timeLeft > (wallrunTime - 0.4f) && vertAxisInput > 0)
            {
                time.rigidbody.AddForce(transform.up * wallRunUpForce, ForceMode.Acceleration);
            }
            // provide downwards boost for last 0.75 secs
            else if (timeLeft < 0.75f)
            {
                time.rigidbody.AddForce(transform.up * -wallRunDownForce, ForceMode.Acceleration);
            }

            if (wishDir.y < -0.3)
            {
                wishDir.y *= 2f;
            }

            Vector3 horizontalDamping = new Vector3(-time.rigidbody.velocity.x, 0, -time.rigidbody.velocity.z);
            if (Vector3.Dot(horizontalDamping, new Vector3(wishDir.x, 0, wishDir.z)) > 0f)
            {
                horizontalDamping *= (horizontalDamping - wishDir).magnitude;
            }
            else
            {
                horizontalDamping = Vector3.ClampMagnitude(horizontalDamping, 3);
            }

            if (wallrunTimer.FractionLeft() < 0.33)
            {
                antiGravityForce *= wallrunTimer.FractionLeft();
            }
            if (distance.magnitude > wallStickDistance) distance = Vector3.zero;


            //Adding forces
            time.rigidbody.AddForce(antiGravityForce);
            time.rigidbody.AddForce(distance.normalized * wallStickiness * Mathf.Clamp(distance.magnitude / wallStickDistance, 0, 1), ForceMode.Acceleration);
            time.rigidbody.AddForce(horizontalDamping, ForceMode.Acceleration);

            RotatePlayerParallelToWall();


            // Added float will increase speed in direction of movement when wall running (it's nice to get a boost from a wall)
            // AND if player speed is currently low when attaching to wall, increase that boost until player hits min speed, then add normal boost
            if (currSpeed * 100 <= wallRunMinSpeed && vertAxisInput > 0)
            {
                time.rigidbody.AddForce(wishDir * 9f);
            }
            else if (vertAxisInput >= 1)
            {
                time.rigidbody.AddForce(wishDir * 1.5f);
            }


            if (Vector3.Dot(time.rigidbody.velocity, transform.forward) < 5)
            {
                time.rigidbody.AddForce(-transform.up * 50);
            }


        }
        if (!grounded)
        {
            wallDetachTimer.StartTimer();
            EnterFlying();
        }
    }


    private void AirMove(Vector3 wishDir, float maxSpeed, float Acceleration)
    {
        if (jump)
        {
            DoubleJump(wishDir);
        }
        if (LedgeClimb())
        {
            EnterClimbing();
        }
        if (wishDir.magnitude != 0)
        {
            Vector3 spid = new Vector3(time.rigidbody.velocity.x, 0, time.rigidbody.velocity.z);

            Vector3 projVel = Vector3.Project(time.rigidbody.velocity, wishDir);

            bool isAway = Vector3.Dot(wishDir, projVel) <= 0f;

            if (projVel.magnitude < maxSpeed || isAway)
            {
                Vector3 vc = wishDir.normalized * Acceleration;

                if (!isAway)
                {
                    vc = Vector3.ClampMagnitude(vc, maxSpeed - projVel.magnitude);
                }
                else
                {
                    vc = Vector3.ClampMagnitude(vc, maxSpeed + projVel.magnitude);
                }
                Vector2 force = ClampedAdditionVector(new Vector2(spid.x, spid.z) * 2f, new Vector2(vc.x, vc.z) * 2f);
                Vector3 sum1 = spid + vc;
                Vector3 sum2 = spid + new Vector3(force.x, 0, force.y);
                if (sum1.magnitude > sum2.magnitude && spid.magnitude > airSpeed)
                {
                    vc.x = force.x;
                    vc.z = force.y;
                }
                time.rigidbody.AddForce(vc, ForceMode.VelocityChange);

                if (!camSwitch.aiming)
                {
                    // rotate player in direction of walking relative to camera
                    float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocityAir, 0.3f);
                    transform.rotation = Quaternion.Euler(0f, angle, 0f);
                }

            }
        }
        if (grounded)
        {
            EnterWalking();
        }
        if(crouched)
        {
            StartCrouch();
        }
    }

    private void Walk(Vector3 wishDir, float maxSpeed, float Acceleration)
    {
        canDJump = true;
        if (jump)
        {
            Jump();
            EnterFlying();
        }
        else if (running)
        {
            maxSpeed *= runSpeedMod;
            //un-toggle running if not moving
            if (wishDir == Vector3.zero) running = false;
        }
        if (LedgeClimb())
        {
            EnterClimbing();
        }
        if (!sliding || sliding && time.rigidbody.velocity.magnitude < 4f)
        {
            sliding = false;
            if (crouched)
            {
                StartCrouch();
            }
            if (crouching)
            {
                maxSpeed *= crouchingModifer;
            }
            wishDir = wishDir.normalized;

            spid = new Vector3(time.rigidbody.velocity.x, 0, time.rigidbody.velocity.z);
            spid = wishDir * maxSpeed - spid;
            if (spid.magnitude < maxSpeed)
            {
                Acceleration *= spid.magnitude / maxSpeed;
            }
            else
            {
                if (new Vector3(time.rigidbody.velocity.x, 0, time.rigidbody.velocity.z).magnitude > airTargetSpeed * 0.9f && !bhopLenTimer.Done())
                {
                    Acceleration = 0;
                }
                else
                {
                    Acceleration *= 0.5f * maxSpeed / spid.magnitude;
                }
            }
            spid = spid.normalized * Acceleration;
            float magn = spid.magnitude;
            spid = Vector3.ProjectOnPlane(spid, groundNormal);
            spid = spid.normalized;
            spid *= magn;
            time.rigidbody.AddForce(spid);

            if (!camSwitch.aiming)
            {
                // rotate player in direction of walking relative to camera
                float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocityWalk, 0.1f);
                transform.rotation = Quaternion.Euler(0f, angle, 0f);
            }
        }
    }

    private void Climb()
    {
        // if not above ledge
        Vector3 currentPos = transform.position;
        //Vector3 direction = (climbTargetPos - transform.position).normalized;

        //if (currentPos != climbTargetPos)
        if (Vector3.Distance(transform.position, climbTargetPos) > 0.001f)
        {
            playerInput = false;
            climbRef.isKinematic = true;
            //climbRef.MovePosition(transform.position + direction * 6 * Time.fixedDeltaTime);
            float step = 3 * Time.fixedDeltaTime; // calculate distance to move
            transform.position = Vector3.MoveTowards(currentPos, climbTargetPos, step);
        }
        else
        {
            climbRef.isKinematic = false;
            playerInput = true;
            EnterWalking();
        }

    }

    private void Slide()
    {
        sliding = true;
        pm.material.dynamicFriction = 0f;
        pm.material.frictionCombine = PhysicMaterialCombine.Minimum;
        time.rigidbody.AddForce(cam.transform.forward * slideForce);
    }

    private void StartCrouch()
    {
        // only want this to be true the momment crouch is pressed
        crouched = false;

        if (grounded && time.rigidbody.velocity.magnitude > 6f)
        {
            Slide();
        }
        if (!grounded)
        {
            crouchedInAir = true;
        }
        crouching = true;
    }

    private void StopCrouch()
    {

        // if player was sliding, reset fov
        if (sliding)
        {
            sliding = false;
        }

        if (crouching) crouching = false;
        if (crouchedInAir) crouchedInAir = false;
    }

    private void Jump()
    {
        if (state == State.Walking && jumpTimer.Done())
        {
            //SoundManagerScript.PlaySound("jump");
            appliedJump = true;
            time.rigidbody.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
            EnterFlying();
            jumpTimer.StartTimer();
        }
    }


    private void DoubleJump(Vector3 wishDir)
    {
        // grace timer check
        bool graceTime = jumpGraceTimer.Done();

        if (canDJump || !graceTime)
        {
            //SoundManagerScript.PlaySound("djump");
            float tempJumpForce = jumpForce;
            //Calculate upwards
            float upSpeed = time.rigidbody.velocity.y;
            if (upSpeed < 0){
                upSpeed = 0;
            }
            else if(upSpeed < dJumpBaseSpd)
            {
                upSpeed = dJumpBaseSpd;
                tempJumpForce = 0;
            }

            //Calculate sideways
            Vector3 jumpVector = Vector3.zero;
            Vector2 force = Vector2.zero;
            wishDir = wishDir.normalized;
            Vector3 spid = new Vector3(time.rigidbody.velocity.x, 0, time.rigidbody.velocity.z);
            if(spid.magnitude < airTargetSpeed)
            {
                jumpVector = wishDir * dashForce;
                jumpVector -= spid;
            }
            else if (Vector3.Dot(spid.normalized, wishDir) > -0.4f)
            {
                jumpVector = wishDir * dashForce;
                force = ClampedAdditionVector(new Vector2(time.rigidbody.velocity.x, time.rigidbody.velocity.z), new Vector2(jumpVector.x, jumpVector.z));
                jumpVector.x = force.x;
                jumpVector.z = force.y;
            }
            else
            {
                jumpVector = wishDir * spid.magnitude;
            }

            //Apply Jump
            jumpVector.y = tempJumpForce;
            time.rigidbody.velocity = new Vector3(time.rigidbody.velocity.x, upSpeed, time.rigidbody.velocity.z);
            time.rigidbody.AddForce(jumpVector, ForceMode.Impulse);
            appliedDoubleJump = true;

            if (graceTime)
            {
                // only use double jump if this is NOT a grace timer jump SO graceTime == true / graceTimer is DONE
                canDJump = false;
            }
            else
            {
                // we used grace jump so end timer/remove grace jump
                jumpGraceTimer.done = true;
            }

            
        }
    }

    private bool LedgeClimb()
    {
        bool hitLedge = false;
        Vector3 topPos = new Vector3(transform.position.x, transform.position.y + pm.height / 2.5f, transform.position.z);
        Vector3 middlePos = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        Vector3 bottomPos = new Vector3(transform.position.x, transform.position.y - pm.height / 2.5f, transform.position.z);

        lookAngle = lookAngle * Mathf.Deg2Rad;
        //rayDir = new Vector3(Mathf.Sin(lookAngle), 0, Mathf.Cos(lookAngle));
        rayDir = transform.forward;

        RaycastHit hit;
        RaycastHit hit2;
        if (Physics.Raycast(bottomPos, rayDir, out hit, 0.75f) || Physics.Raycast(middlePos, rayDir, out hit2, 0.5f) && (hit.transform.tag == "Ground" && hit2.transform.tag == "Ground"))
        {
            // if bottom hits and top does not
            RaycastHit hit1;
            if (!Physics.Raycast(topPos, rayDir, out hit1, 0.75f))
            {
                    hitLedge = true;
            }
            else
            {
                hitLedge = false;
            }

        }
        return hitLedge;
    }

    private void RotatePlayerParallelToWall()
    {
        float camToWallAngle = CamAngleToWall();
        float playerToWallAngle = PlayerAngleToWall();

        // fix for jitter
        if (playerToWallAngle <= -87 && playerToWallAngle >= -95)
        {
            playerToWallAngle = -90;
        }
        else if (playerToWallAngle >= 87 && playerToWallAngle <= 95)
        {
            playerToWallAngle = 90;
        }
        if (playerToWallAngle == 0) playerToWallAngle = 1;

        // for animations
        angleToWall = playerToWallAngle;

        if (camToWallAngle < -90 && camToWallAngle >= -179) // rotate player's forward right (relative to looking straight at wall)
        {

            if (playerToWallAngle != -90)
            {
                //lerp player y rotation to -90 degrees
                transform.eulerAngles = new Vector3(
                transform.eulerAngles.x,
                transform.eulerAngles.y + 7,
                transform.eulerAngles.z);
            }
        }
        else if (camToWallAngle >= 90 && camToWallAngle <= 180) //rotate player's forward left ^^
        {

            
            if (playerToWallAngle != 90)
            {
                //lerp player y rotation to 90 degrees
                transform.eulerAngles = new Vector3(
                transform.eulerAngles.x,
                transform.eulerAngles.y - 7,
                transform.eulerAngles.z);
            }
        }
    }

    #endregion



    #region MathGenious

    private Vector2 ClampedAdditionVector(Vector2 a, Vector2 b)
    {
        float k, x, y;
        k = Mathf.Sqrt(Mathf.Pow(a.x, 2) + Mathf.Pow(a.y, 2)) / Mathf.Sqrt(Mathf.Pow(a.x + b.x, 2) + Mathf.Pow(a.y + b.y, 2));
        x = k * (a.x + b.x) - a.x;
        y = k * (a.y + b.y) - a.y;
        return new Vector2(x, y);
    }

    private Vector3 RotateToPlane(Vector3 vect, Vector3 normal)
    {
        Vector3 rotDir = Vector3.ProjectOnPlane(normal, Vector3.up);
        Quaternion rotation = Quaternion.AngleAxis(-90f, Vector3.up);
        rotDir = rotation * rotDir;
        float angle = -Vector3.Angle(Vector3.up, normal);
        rotation = Quaternion.AngleAxis(angle, rotDir);
        vect = rotation * vect;
        return vect;
    }

    private float WallrunCameraAngle()
    {
        Vector3 rotDir = Vector3.ProjectOnPlane(groundNormal, Vector3.up);
        Quaternion rotation = Quaternion.AngleAxis(-90f, Vector3.up);
        rotDir = rotation * rotDir;
        float angle = Vector3.SignedAngle(Vector3.up, groundNormal, Quaternion.AngleAxis(90f, rotDir) * groundNormal);
        angle -= 90;
        angle /= 180;
        Vector3 playerDir = transform.forward;
        Vector3 normal = new Vector3(groundNormal.x, 0, groundNormal.z);

        return Vector3.Cross(playerDir, normal).y * angle;
    }

    private bool CanRunOnThisWall(Vector3 normal)
    {
        if(Vector3.Angle(normal, groundNormal) > 10 || wallBanTimer.Done())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private float CamAngleToWall()
    {
        RaycastHit hit;
       if (Physics.Raycast(transform.position + Vector3.up, -groundNormal, out hit, wallStickDistance))
       {
            float angle = Vector3.Angle(hit.normal, cam.forward);
            Vector3 cross = Vector3.Cross(hit.normal, cam.forward);
            if (cross.y < 0) angle = -angle;
            return angle;
       }
        else
        {
            return 0;
        }

    }

    private float PlayerAngleToWall()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, -groundNormal, out hit, wallStickDistance))
        {
            float angle = Vector3.Angle(hit.normal, transform.forward);
            Vector3 cross = Vector3.Cross(hit.normal, transform.forward);
            if (cross.y < 0) angle = -angle;
            return angle;
        }
        else
        {
            return 0;
        }
    }

    private Vector3 VectorToWall()
    {
        Vector3 direction;
        Vector3 position = transform.position + Vector3.up * 0.1f;
        RaycastHit hit;
        if (Physics.Raycast(position, -groundNormal, out hit, wallStickDistance) && Vector3.Angle(groundNormal, hit.normal) < 70)
        {
            groundNormal = hit.normal;
            Physics.Raycast(position, -groundNormal, out hit, wallStickDistance);
            direction = hit.point - position;
            return direction;
        }
        else
        {
            return Vector3.positiveInfinity;
        }
    }

    private Vector3 VectorToGround()
    {
        Vector3 position = transform.position;
        RaycastHit hit;
        if (Physics.Raycast(position, Vector3.down, out hit, wallStickDistance))
        {
            return hit.point - position;
        }
        else
        {
            return Vector3.positiveInfinity;
        }
    }

    private float CurrentPlayerSpeed()
    {
        currSpeed = (transform.position - lastPosition).magnitude;
        lastPosition = transform.position;

        return currSpeed;
    }
    #endregion
}
