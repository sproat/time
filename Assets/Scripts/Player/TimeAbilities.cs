﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Chronos;
using System;

public class TimeAbilities : MonoBehaviour
{
    // Values
    [Tooltip("Must be lower than Timeline.recordingDuration")]
    public int rewindSeconds;
    [Tooltip("1 = normal speed | 2 = 2x rewind speed")]
    public int rewindSpeedMultiplier;

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.E))
        {
            StartCoroutine(Rewind());
        }
    }

    IEnumerator Rewind()
    {
        Clock clock = Timekeeper.instance.Clock("Root");

        clock.localTimeScale = -rewindSpeedMultiplier;
        yield return new WaitForSeconds(rewindSeconds);
        clock.localTimeScale = 1;

    }
}
