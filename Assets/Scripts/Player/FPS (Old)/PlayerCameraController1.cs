﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Chronos;

public class PlayerCameraController1 : MonoBehaviour
{
    public float sensX;
    public float sensY;

    private GameObject player;
    private Vector2 smoothedVel;
    [HideInInspector] public Vector2 currentLook;
    private float curTilt = 0;
    private float wishTilt = 0;
    private float delta;
    private float wishFov = 90;
    private float addedFov = 90;
    private float fov = 90;
    public Camera cam;
    private Timeline timeline;

    void Awake()
    {
        timeline = GetComponentInParent<Timeline>();
        curTilt = transform.localEulerAngles.z;
        player = transform.parent.gameObject;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    void Update()
    {
        // Seemingly changes FOV based on velocity of player 
        //addedFov = Mathf.Clamp(time.rigidbody.velocity.magnitude - 3.44f, 0f, 180f - fov);
        //fov = Mathf.Lerp(fov, wishFov + addedFov, Time.deltaTime);
        //cam.fieldOfView = fov;

        if (timeline.timeScale > 0)
        {
            curTilt = transform.localEulerAngles.z;
            if (curTilt < 180)
            {
                delta = Mathf.Abs(wishTilt - curTilt);
            }
            else
            {
                delta = Mathf.Abs(360 - wishTilt - curTilt);
            }
            delta /= 2;
            curTilt = Mathf.LerpAngle(curTilt, wishTilt, timeline.deltaTime * delta);
            RotateCamera();
        }
    }

    void RotateCamera()
    {

            Vector2 mouseInput = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));
            mouseInput.x *= sensX;
            mouseInput.y *= sensY;

            currentLook.x += mouseInput.x;
            currentLook.y = Mathf.Clamp(currentLook.y += mouseInput.y, -90, 90);

            transform.localRotation = Quaternion.AngleAxis(-currentLook.y, Vector3.right);
            transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y, curTilt);
            player.transform.localRotation = Quaternion.AngleAxis(currentLook.x, player.transform.up);

        
    }

    #region Setters

    public void SetTilt(float newVal)
    {
        wishTilt = newVal;

    }

    public void SetXSens(float newVal)
    {
        sensX = newVal;
    }

    public void SetYSens(float newVal)
    {
        sensY = newVal;
    }

    public void SetFov(float newVal)
    {
        wishFov = newVal;
    }
    #endregion
}
