﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Chronos;

public class PlayerController1 : MonoBehaviour
{
    //Ground
    [Header("Ground")]
    public float groundSpeed = 3.44f;
    public float runSpeedMod = 1.5f;
    public float crouchingModifer = 0.5f;
    public float grAccel = 200f;

    //Air
    [Header("Air")]
    public float airSpeed = 3f;
    public float airAccel = 0.3f;
    public float airTargetSpeed = 15f;

    //Wall
    [Header("Wall")]
    public float wallAccel = 5f;
    public float wallStickiness = 10f;
    public float wallStickDistance = 0.5f;
    public float wallRunTilt = 15f;
    public float wallFloorBarrier = 40f;
    public float wallBanTime = 0.3f;
    public float wallrunTime = 4f;
    public float wallRunJumpDampeningModifer = 0.6f;
    public float wallRunVertDamp = 0.6f;
    public float wallRunMinSpeed = 15f;
    public float wallRunUpForce = 1000f;
    public float wallRunDownForce = 1000f;

    //Jump
    [Header("Jump")]
    public float jumpForce = 6f;
    public float dashForce = 4f;
    public float bhopLeniency = 0.1f;
    public float dJumpBaseSpd = 8;

    //Crouch & Slide
    [Header("Crouch & Slide")]
    public float slideForce = 400;
    public float slideCounterMovement = 0.2f;
    public float slideFovIncrease = 10;
    public float camLerpSpeed = 10f;
    private float oldFovValue = 90;
    private float slideFovValue = 10f;

    private Vector3 crouchScale = new Vector3(1, 0.5f, 1);
    private Vector3 playerScale;

    // Private || Not Shown in inspector
    bool jump;
    [HideInInspector] public bool canDJump = false;

    bool running = false;
    bool grounded = false;
    bool sliding = false;
    bool crouchedInAir = false;
    bool graceTimerStarted = false;
    bool inAir = false;
    bool appliedJump = false;
    bool appliedDoubleJump = false;
    float angleToWall;

    [HideInInspector] public bool crouched = false;
    [HideInInspector] public bool crouching = false;

    Vector3 groundNormal;
    Vector3 bannedGroundNormal;

    Vector3 dir;
    Vector3 spid;
    Vector3 lastPosition = Vector3.zero;

    float currSpeed = 0;
    float vertAxisInput = 0;
    float horizAxisInput = 0;
    float lerpT = 0;
    float dynamFricOld;
    private enum State
    {
        Walking,
        Flying,
        Wallruning
    }
    private State state;

    //Timers
    private Timer bhopLenTimer;
    private Timer wallrunTimer;
    private Timer wallDetachTimer;
    private Timer wallBanTimer;
    private Timer jumpTimer;
    private Timer jumpGraceTimer;

    //GameObjects/References
    private Timeline time;
    private CapsuleCollider pm;
    private PlayerCameraController camCon;
    private Timeline timeline;
    private Animator animate;

    [Header("Public References")]
    public Collider wallRunCollider;
    public Camera cam;


    void Awake()
    {
        //Timers
        bhopLenTimer = gameObject.AddComponent<Timer>();
        bhopLenTimer.SetTime(bhopLeniency);

        wallBanTimer = gameObject.AddComponent<Timer>();
        wallBanTimer.SetTime(wallBanTime);

        wallDetachTimer = gameObject.AddComponent<Timer>();
        wallDetachTimer.SetTime(0.2f);

        wallrunTimer = gameObject.AddComponent<Timer>();
        wallrunTimer.SetTime(wallrunTime);

        jumpTimer = gameObject.AddComponent<Timer>();
        jumpTimer.SetTime(0.05f);

        jumpGraceTimer = gameObject.AddComponent<Timer>();
        jumpGraceTimer.SetTime(0.4f);


        //GameObjects
        camCon = cam.GetComponent<PlayerCameraController>();
        time = GetComponent<Timeline>();
        pm = GetComponent<CapsuleCollider>();
        camCon.SetTilt(0);
        timeline = GetComponent<Timeline>();
        animate = GetComponentInChildren<Animator>();

        //Setting up beggining state
        EnterFlying();
        canDJump = true;
        pm.material.dynamicFriction = 0;
        pm.material.frictionCombine = PhysicMaterialCombine.Minimum;
        playerScale = transform.localScale;

        oldFovValue = cam.fieldOfView;
        slideFovValue += oldFovValue;
        dynamFricOld = pm.material.dynamicFriction;
    }

    void Update()
    {
        if (time.timeScale > 0)
        {
            dir = Direction();
            CrouchBoostJump();
            SetAnimation();
        }
    }


    private void FixedUpdate()
    {

        if (time.timeScale > 0)
        {
            if (wallDetachTimer.Done() && !wallBanTimer.Done())
            {
                bannedGroundNormal = groundNormal;
            }
            else
            {
                bannedGroundNormal = Vector3.zero;
            }
            switch (state)
            {
                case State.Wallruning:
                    float camAngle = WallrunCameraAngle();
                    angleToWall = camAngle;
                    camCon.SetTilt(camAngle * wallRunTilt);
                    Wallrun(dir, groundSpeed, grAccel);
                    break;
                case State.Walking:
                    camCon.SetTilt(0);
                    Walk(dir, groundSpeed, grAccel);
                    break;
                case State.Flying:
                    camCon.SetTilt(0);
                    AirMove(dir, airSpeed, airAccel);
                    break;
            }
            jump = false;

            if (sliding)
            {
                //add counterforce
                time.rigidbody.AddForce(timeline.deltaTime * -time.rigidbody.velocity.normalized * slideCounterMovement);
            }
            else
            {
                pm.material.dynamicFriction = dynamFricOld;
                pm.material.frictionCombine = PhysicMaterialCombine.Average;
            }
            currSpeed = CurrentPlayerSpeed();
        }


    }

    void OnCollisionStay(Collision collision)
    {
        if (collision.contactCount > 0)
        {
            float angle;
            foreach (ContactPoint contact in collision.contacts)
            {
                angle = Vector3.Angle(contact.normal, Vector3.up);
                if(angle < wallFloorBarrier)
                {
                    EnterWalking();
                    grounded = true;
                    groundNormal = contact.normal;
                    return;
                }
            }
            if (VectorToGround().magnitude > 0.2f)
            {
                grounded = false;
            }
            if (grounded == false)
            {
                foreach (ContactPoint contact in collision.contacts)
                {
                    if (contact.thisCollider == wallRunCollider && state != State.Walking)
                    {
                        angle = Vector3.Angle(contact.normal, Vector3.up);
                        if (angle > wallFloorBarrier && angle < 120f)
                        {
                            grounded = true;
                            groundNormal = contact.normal;
                            EnterWallrun();
                            return;
                        }
                    }
                }
            }
        }
    }

    void OnCollisionExit(Collision collision)
    {
        if (collision.contactCount == 0)
        {
            EnterFlying();
        }
    }

    private Vector3 Direction()
    {
        // prevent issues that arise when player uses horizontal innput while wall running
        if (state == State.Wallruning)
        {
            horizAxisInput = 0;
            vertAxisInput = Mathf.Clamp(Input.GetAxisRaw("Vertical"), 0, 1);
        }
        else
        {
            horizAxisInput = Input.GetAxisRaw("Horizontal");
            vertAxisInput = Input.GetAxisRaw("Vertical");
        }
            
        
        

        Vector3 direction = new Vector3(horizAxisInput, 0, vertAxisInput);
        return time.rigidbody.component.transform.TransformDirection(direction);
    }

    private void SetAnimation()
    {
        float moveV = vertAxisInput;
        float moveH = horizAxisInput;
        // player walking
       // if (state == State.Walking)
      //  {
            //animate.SetBool("landed", false);


            // if player is sliding
            if (sliding)
            {
                animate.SetBool("sliding", true);
            }
            else if (!sliding)
            {
                animate.SetBool("sliding", false);
            }
            else
            {

                // if player is crouching
                if (crouching)
                {
                    animate.SetBool("crouching", true);
                }
                else if (!crouching)
                {
                    animate.SetBool("crouching", false);
                }
            }


            // if player is not crouching/sliding and is running
            if ( !crouching && !sliding && running)
            {
                    moveV += 1;
            }
       // }
        if (state == State.Wallruning)
        {
            if (angleToWall < 0)
            {
                animate.SetFloat("wallAngle", 1);
            }
            else
            {
                animate.SetFloat("wallAngle", 2);
            }

        }
        else
        {
            animate.SetFloat("wallAngle", 0);
        }

        if (appliedJump)
        {
            //animate.SetBool("jump", true);
            animate.SetFloat("jumps", 1f);
            Debug.Log(animate.GetFloat("jumps"));
            appliedJump = false;
        }

        if (appliedDoubleJump) //this means we used double jump
        {
            //animate.SetTrigger("djump");
            animate.SetFloat("jumps", 2f);
            animate.SetTrigger("doubleJumpUsed");
            appliedDoubleJump = false;
        }

        if (!grounded)
        {
            inAir = true;
            animate.SetBool("grounded", false);
        }
        else
        {
            animate.SetBool("grounded", true);

        }

        // check for landing on ground separtly from states since this can occur at various times
        if (grounded && inAir)
        {
            //this checks if we are grounded but the lended check knows we were also just in the air
            inAir = false;
            animate.SetFloat("jumps", 0);
            //animate.SetBool("landed", true);
            //animate.SetBool("jump", false);
        }

        animate.SetFloat("vertical_f", moveV, 1f, Time.deltaTime * 10f);
        animate.SetFloat("horizontal_f", moveH, 1f, Time.deltaTime * 10f);


    }

    #region EnterState
    private void EnterWalking()
    {
        if (state != State.Walking)
        {
            // start slide if crouching in-air when you hit the ground
            if (crouching && time.rigidbody.velocity.magnitude > 0.5f)
            {
                Slide();
            }

            //SoundManagerScript.PlaySound("land");
            bhopLenTimer.StartTimer();
            state = State.Walking;
        }
    }

    private void EnterFlying( bool wishFly = false)
    {
        grounded = false;
        if(state == State.Wallruning && VectorToWall().magnitude < wallStickDistance && !wishFly)
        {
            return;
        }
        else if (state != State.Flying)
        {
            wallBanTimer.StartTimer();
            canDJump = true;
            state = State.Flying;
        }
    }

    private void EnterWallrun()
    {
        if (state != State.Wallruning && VectorToGround().magnitude > 0.2f && CanRunOnThisWall(bannedGroundNormal) && wallDetachTimer.Done())
        {
            wallrunTimer.StartTimer();
            canDJump = true;
            state = State.Wallruning;

            // remove y-axis velocity when grabbing on a wall
            Vector3 tmpVelocity = time.rigidbody.velocity;
            tmpVelocity.y = 0f;
            time.rigidbody.velocity = tmpVelocity;

        }
    }

    #endregion



    #region Movement

    private void CrouchBoostJump()
    {
        if (grounded && graceTimerStarted)
        {
            // reset grace timer when we hit the ground
            graceTimerStarted = false;
        }

        // Jumping
        if (Input.GetKeyDown(KeyCode.Space))
        {
            jump = true;
            if (grounded)
            {
                graceTimerStarted = true;
                jumpGraceTimer.done = true;
            }

        }
        else if (!grounded && !graceTimerStarted)
        {
            // here we want to add a "grace timer" to allow a jump after the player walks off a ledge without using their double jump
            jumpGraceTimer.StartTimer();
            graceTimerStarted = true;
        }

        // Sprinting (toggle)
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            if (running) running = false;
            else
            {
                running = true;
            }
            
        }

        //Crouching
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            //this looking confusing but the code needs to know when the button is pressed AND if it's being held down
            // bc of in-air crouching and sliding

            crouched = true;
            crouching = true;
        }
        if (Input.GetKeyUp(KeyCode.LeftControl))
        {
            StopCrouch();
            crouching = false;
        }
        // this is to check for sliding when you hit the ground since it only checks once when you crouch (oops..)
        if (crouchedInAir && crouching && !sliding && time.rigidbody.velocity.magnitude > groundSpeed)
        {
            Slide();
            crouchedInAir = false;
        }
    }

    private void Wallrun(Vector3 wishDir, float maxSpeed, float Acceleration)
    {
        float rawAngle = AngleToWall();
        float angle = Mathf.Abs(rawAngle);

        if (jump)
        {
            Vector3 direction = new Vector3(groundNormal.x, 1, groundNormal.z);

            // wallRunJumpDampeningModifer slows down a jump from a wall, was too fast
            time.rigidbody.AddForce(direction * jumpForce * wallRunJumpDampeningModifer, ForceMode.Impulse);
            EnterFlying(true);
        }
        else if (wallrunTimer.Done())
        {
            //time.rigidbody.AddForce(groundNormal * 3f, ForceMode.Impulse);

            // Applies force to eject player off of wall in opposite direction to wall
            // This is difference to default approach which pushes player down

            time.rigidbody.AddForce(-VectorToWall() * (Mathf.Clamp(currSpeed * 6, 1, 3) * 20), ForceMode.Impulse);
            EnterFlying(true);
        }
        else if (angle < 50 )
        {
             // eject from wall and change state to flying
                time.rigidbody.AddForce(-VectorToWall() * (Mathf.Clamp(currSpeed * 6, 1, 3) * 20), ForceMode.Impulse);
                EnterFlying(true);
        }
        else
        {
            //Variables
            Vector3 distance = VectorToWall();
            wishDir = wishDir.normalized;
            wishDir = RotateToPlane(wishDir, -distance.normalized);
            wishDir *= wallAccel;

            // workaround to fix issue where looking up/down would effect wallrunning direction 
            wishDir = new Vector3(wishDir.x, 0, wishDir.z);

            Vector3 antiGravityForce = -Physics.gravity;
            Vector3 verticalDamping;

            //Calculating forces
            if (Mathf.Sign(time.rigidbody.velocity.y) != Mathf.Sign(wishDir.y) && time.rigidbody.velocity.y < 0)
            {
                verticalDamping = new Vector3(0, -time.rigidbody.velocity.y * 0.1f, 0);
                time.rigidbody.AddForce(verticalDamping * wallRunVertDamp, ForceMode.VelocityChange);
            }
            else
            {
                verticalDamping = new Vector3(0, -time.rigidbody.velocity.y * 0.5f, 0);
                time.rigidbody.AddForce(verticalDamping * wallRunVertDamp, ForceMode.Acceleration);

            }

            float timeLeft = wallrunTimer.CurrentTime();
            //provide upwards boost for first 0.4 secs if player is trying to move forwards
            if (timeLeft > (wallrunTime - 0.4f) && vertAxisInput > 0)
            {
                time.rigidbody.AddForce(transform.up * wallRunUpForce, ForceMode.Acceleration);
            }
            // provide downwards boost for last 0.75 secs
            else if (timeLeft < 0.75f)
            {
                time.rigidbody.AddForce(transform.up * -wallRunDownForce, ForceMode.Acceleration);
            }

            if (wishDir.y < -0.3)
            {
                wishDir.y *= 2f;
            }

            Vector3 horizontalDamping = new Vector3(-time.rigidbody.velocity.x, 0, -time.rigidbody.velocity.z);
            if (Vector3.Dot(horizontalDamping, new Vector3(wishDir.x, 0, wishDir.z)) > 0f)
            {
                horizontalDamping *= (horizontalDamping - wishDir).magnitude;
            }
            else
            {
                horizontalDamping = Vector3.ClampMagnitude(horizontalDamping, 3);
            }

            if (wallrunTimer.FractionLeft() < 0.33)
            {
                antiGravityForce *= wallrunTimer.FractionLeft();
            }
            if (distance.magnitude > wallStickDistance) distance = Vector3.zero;


            //Adding forces
            time.rigidbody.AddForce(antiGravityForce);
            time.rigidbody.AddForce(distance.normalized * wallStickiness * Mathf.Clamp(distance.magnitude / wallStickDistance, 0, 1), ForceMode.Acceleration);
            time.rigidbody.AddForce(horizontalDamping, ForceMode.Acceleration);


            // Added float will increase speed in direction of movement when wall running (it's nice to get a boost from a wall)
            // AND if player speed is currently low when attaching to wall, increase that boost until player hits min speed, then add normal boost
            if (currSpeed * 100 <= wallRunMinSpeed && vertAxisInput > 0)
            {
                time.rigidbody.AddForce(wishDir * 9f);
            }
            else if (vertAxisInput >= 1)
            {
                time.rigidbody.AddForce(wishDir * 1.5f);
            }


            // TF2: if player velocity is under certain ammount AND player input is less than 0
            //  (AKA they are not trying to move) player is pulled towards ground
            //if (currSpeed * 100 <= wallRunMinSpeed && vertAxisInput < 0)
            // {
            //     time.rigidbody.AddForce(-transform.up * 500);
            // }

            if (Vector3.Dot(time.rigidbody.velocity, transform.forward) < 5)
            {
                time.rigidbody.AddForce(-transform.up * 50);
            }


        }
        if (!grounded)
        {
            wallDetachTimer.StartTimer();
            EnterFlying();
        }
    }

    private void AirMove(Vector3 wishDir, float maxSpeed, float Acceleration)
    {
        if (jump)
        {
            DoubleJump(wishDir);
        }
        if (wishDir.magnitude != 0)
        {
            Vector3 spid = new Vector3(time.rigidbody.velocity.x, 0, time.rigidbody.velocity.z);

            Vector3 projVel = Vector3.Project(time.rigidbody.velocity, wishDir);

            bool isAway = Vector3.Dot(wishDir, projVel) <= 0f;

            if (projVel.magnitude < maxSpeed || isAway)
            {
                Vector3 vc = wishDir.normalized * Acceleration;

                if (!isAway)
                {
                    vc = Vector3.ClampMagnitude(vc, maxSpeed - projVel.magnitude);
                }
                else
                {
                    vc = Vector3.ClampMagnitude(vc, maxSpeed + projVel.magnitude);
                }
                Vector2 force = ClampedAdditionVector(new Vector2(spid.x, spid.z) * 2f, new Vector2(vc.x, vc.z) * 2f);
                Vector3 sum1 = spid + vc;
                Vector3 sum2 = spid + new Vector3(force.x, 0, force.y);
                if (sum1.magnitude > sum2.magnitude && spid.magnitude > airSpeed)
                {
                    vc.x = force.x;
                    vc.z = force.y;
                }
                time.rigidbody.AddForce(vc, ForceMode.VelocityChange);
            }
        }
        if (grounded)
        {
            EnterWalking();
        }
        if(crouched)
        {
            StartCrouch();
        }
    }

    private void Walk(Vector3 wishDir, float maxSpeed, float Acceleration)
    {
        canDJump = true;
        if (jump)
        {
            Jump();
            EnterFlying();
        }
        else if (running)
        {
            maxSpeed *= runSpeedMod;
            //un-toggle running if not moving
            if (wishDir == Vector3.zero) running = false;
        }
        if (!sliding || sliding && time.rigidbody.velocity.magnitude < 4f)
        {
            sliding = false;
            if (crouched)
            {
                StartCrouch();
            }
            if (crouching)
            {
                maxSpeed *= crouchingModifer;
            }
            wishDir = wishDir.normalized;
            spid = new Vector3(time.rigidbody.velocity.x, 0, time.rigidbody.velocity.z);
            spid = wishDir * maxSpeed - spid;
            if (spid.magnitude < maxSpeed)
            {
                Acceleration *= spid.magnitude / maxSpeed;
            }
            else
            {
                if (new Vector3(time.rigidbody.velocity.x, 0, time.rigidbody.velocity.z).magnitude > airTargetSpeed * 0.9f && !bhopLenTimer.Done())
                {
                    Acceleration = 0;
                }
                else
                {
                    Acceleration *= 0.5f * maxSpeed / spid.magnitude;
                }
            }
            spid = spid.normalized * Acceleration;
            float magn = spid.magnitude;
            spid = Vector3.ProjectOnPlane(spid, groundNormal);
            spid = spid.normalized;
            spid *= magn;
            time.rigidbody.AddForce(spid);
        }
    }

    private void Slide()
    {
        sliding = true;
        pm.material.dynamicFriction = 0f;
        pm.material.frictionCombine = PhysicMaterialCombine.Minimum;
        time.rigidbody.AddForce(cam.transform.forward * slideForce);
    }

    private void StartCrouch()
    {
        // only want this to be true the momment crouch is pressed
        crouched = false;

        //transform.localScale = crouchScale;
        //transform.position = new Vector3(transform.position.x, transform.position.y - 0.5f, transform.position.z);

        if (grounded && time.rigidbody.velocity.magnitude > 1f)
        {
            Slide();

            if (cam.fieldOfView != slideFovValue)
            {
                // increase fov
                cam.fieldOfView = slideFovValue;
            }
        }
        // This is for increaseing fov value mid-air when velocity is met but were not technically sliding yet
        else if (time.rigidbody.velocity.magnitude > 0.5f)                
        {
            // lerp increase fov
            StartCoroutine(LerpFOV(cam.fieldOfView, slideFovValue, camLerpSpeed));
        }
        if (!grounded)
        {
            crouchedInAir = true;
        }
    }

    private void StopCrouch()
    {
        //transform.localScale = playerScale;
        //transform.position = new Vector3(transform.position.x, transform.position.y + 0.5f, transform.position.z);
        //camCon.SetTilt(0);

        // if player was sliding, reset fov
        if (sliding)
        {
            sliding = false;
            // lerp decrease fov
            StartCoroutine(LerpFOV(cam.fieldOfView, oldFovValue, camLerpSpeed));
        }
        else if (cam.fieldOfView != oldFovValue) cam.fieldOfView = oldFovValue;

        if (crouching) crouching = false;
        if (crouchedInAir) crouchedInAir = false;
    }

    private void Jump()
    {
        if (state == State.Walking && jumpTimer.Done())
        {
            //SoundManagerScript.PlaySound("jump");
            appliedJump = true;
            time.rigidbody.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
            EnterFlying();
            jumpTimer.StartTimer();
        }
    }


    private void DoubleJump(Vector3 wishDir)
    {
        // grace timer check
        bool graceTime = jumpGraceTimer.Done();

        if (canDJump || !graceTime)
        {
            //SoundManagerScript.PlaySound("djump");
            float tempJumpForce = jumpForce;
            //Calculate upwards
            float upSpeed = time.rigidbody.velocity.y;
            if (upSpeed < 0){
                upSpeed = 0;
            }
            else if(upSpeed < dJumpBaseSpd)
            {
                upSpeed = dJumpBaseSpd;
                tempJumpForce = 0;
            }

            //Calculate sideways
            Vector3 jumpVector = Vector3.zero;
            Vector2 force = Vector2.zero;
            wishDir = wishDir.normalized;
            Vector3 spid = new Vector3(time.rigidbody.velocity.x, 0, time.rigidbody.velocity.z);
            if(spid.magnitude < airTargetSpeed)
            {
                jumpVector = wishDir * dashForce;
                jumpVector -= spid;
            }
            else if (Vector3.Dot(spid.normalized, wishDir) > -0.4f)
            {
                jumpVector = wishDir * dashForce;
                force = ClampedAdditionVector(new Vector2(time.rigidbody.velocity.x, time.rigidbody.velocity.z), new Vector2(jumpVector.x, jumpVector.z));
                jumpVector.x = force.x;
                jumpVector.z = force.y;
            }
            else
            {
                jumpVector = wishDir * spid.magnitude;
            }

            //Apply Jump
            jumpVector.y = tempJumpForce;
            time.rigidbody.velocity = new Vector3(time.rigidbody.velocity.x, upSpeed, time.rigidbody.velocity.z);
            time.rigidbody.AddForce(jumpVector, ForceMode.Impulse);
            appliedDoubleJump = true;

            if (graceTime)
            {
                // only use double jump if this is NOT a grace timer jump SO graceTime == true / graceTimer is DONE
                canDJump = false;
            }
            else
            {
                // we used grace jump so end timer/remove grace jump
                jumpGraceTimer.done = true;
            }

            
        }
    }

    #endregion



    #region MathGenious

    private Vector2 ClampedAdditionVector(Vector2 a, Vector2 b)
    {
        float k, x, y;
        k = Mathf.Sqrt(Mathf.Pow(a.x, 2) + Mathf.Pow(a.y, 2)) / Mathf.Sqrt(Mathf.Pow(a.x + b.x, 2) + Mathf.Pow(a.y + b.y, 2));
        x = k * (a.x + b.x) - a.x;
        y = k * (a.y + b.y) - a.y;
        return new Vector2(x, y);
    }

    private Vector3 RotateToPlane(Vector3 vect, Vector3 normal)
    {
        Vector3 rotDir = Vector3.ProjectOnPlane(normal, Vector3.up);
        Quaternion rotation = Quaternion.AngleAxis(-90f, Vector3.up);
        rotDir = rotation * rotDir;
        float angle = -Vector3.Angle(Vector3.up, normal);
        rotation = Quaternion.AngleAxis(angle, rotDir);
        vect = rotation * vect;
        return vect;
    }

    private float WallrunCameraAngle()
    {
        Vector3 rotDir = Vector3.ProjectOnPlane(groundNormal, Vector3.up);
        Quaternion rotation = Quaternion.AngleAxis(-90f, Vector3.up);
        rotDir = rotation * rotDir;
        float angle = Vector3.SignedAngle(Vector3.up, groundNormal, Quaternion.AngleAxis(90f, rotDir) * groundNormal);
        angle -= 90;
        angle /= 180;
        Vector3 playerDir = transform.forward;
        Vector3 normal = new Vector3(groundNormal.x, 0, groundNormal.z);

        return Vector3.Cross(playerDir, normal).y * angle;
    }

    private bool CanRunOnThisWall(Vector3 normal)
    {
        if(Vector3.Angle(normal, groundNormal) > 10 || wallBanTimer.Done())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private float AngleToWall()
    {
        Vector3 position = transform.position + Vector3.up * 0.1f;
        RaycastHit hit;
        if (Physics.Raycast(position, -groundNormal, out hit, wallStickDistance))
        {
            float angle = Vector3.Angle(transform.forward, hit.normal);
            return angle;
        }
        else
        {
            return 0;
        }
    }

    private Vector3 VectorToWall()
    {
        Vector3 direction;
        Vector3 position = transform.position + Vector3.up * 0.1f;
        RaycastHit hit;
        if (Physics.Raycast(position, -groundNormal, out hit, wallStickDistance) && Vector3.Angle(groundNormal, hit.normal) < 70)
        {
            groundNormal = hit.normal;
            Physics.Raycast(position, -groundNormal, out hit, wallStickDistance);
            direction = hit.point - position;
            return direction;
        }
        else
        {
            return Vector3.positiveInfinity;
        }
    }

    private Vector3 VectorToGround()
    {
        Vector3 position = transform.position;
        RaycastHit hit;
        if (Physics.Raycast(position, Vector3.down, out hit, wallStickDistance))
        {
            return hit.point - position;
        }
        else
        {
            return Vector3.positiveInfinity;
        }
    }

    private float CurrentPlayerSpeed()
    {
        currSpeed = (transform.position - lastPosition).magnitude;
        lastPosition = transform.position;

        return currSpeed;
    }

    IEnumerator LerpFOV(float currFOV, float targetFOV, float speed)
    {
        lerpT += Time.deltaTime * speed;
        lerpT = Mathf.Clamp(lerpT, 0.0f, 1.0f);
        Camera.main.fieldOfView = Mathf.Lerp(currFOV, targetFOV, lerpT);

        yield return null;
    }
    #endregion



    #region Setters

    public void AdjustGoundSpeed(float newValue)
    {
        groundSpeed = newValue;
    }
    public void AdjustGoundAcceleration(float newValue)
    {
        grAccel = newValue;
    }
    public void AdjustAirSpeed(float newValue)
    {
        airSpeed = newValue;
    }
    public void AdjustAirAccel(float newValue)
    {
        airAccel = newValue;
    }
    public void AdjustJumpForce(float newValue)
    {
        jumpForce = newValue;
    }
    public void AdjustBhopLeniency(float newValue)
    {
        bhopLeniency = newValue;
    }
    public void AdjustAirTargetSpeed(float newValue)
    {
        airTargetSpeed = newValue;
    }
    public void AdjustDashForce(float newValue)
    {
        dashForce = newValue;
    }
    public void AdjustDoubleJumpBaseSpeed(float newValue)
    {
        dJumpBaseSpd = newValue;
    }

    #endregion
}
